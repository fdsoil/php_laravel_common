const sendNivel1 = function (estadoId) {
  const funcResponse = function (response) {
    createNivel(
      response, tmpNivel[1], "llenarNivel( 2, this);", '_t1', 'divNivel1', '300', 'tablaNivel1', '--'
    );
  }
  Service.municipio(estadoId).then(response => {
    funcResponse(response);
  });
}

const sendNivel2 = function (municipioId) {
  const funcResponse = function (response) {
    createNivel(
      response, tmpNivel[2], "selectLugarGeo(this);", '_t2', 'divNivel2', '300', 'tablaNivel2', '---'
    );    
    Service.ciudad(municipioId).then(response => {      
      document.getElementById('id_ciudad').value=response["descripcion"];
    });     
  }  
  Service.parroquia(municipioId).then(response => {  
    funcResponse(response);
  });  
}
