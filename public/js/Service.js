const Service = (function () {
 
  return {  
   
    async municipio(estadoId) {
      let response = await axios.get('municipio/' + estadoId);
      return response.data;
    },    
    
    async parroquia(municipioId) {
      let response = await axios.get('parroquia/' + municipioId);
      return response.data;
    },
    
    async ciudad(municipioId) {
      let response = await axios.get('ciudad/' + municipioId);
      return response.data;
    }
        
  }
})();
