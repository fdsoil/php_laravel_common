<?php

namespace App\Http\Controllers\MeasureUnit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MeasureUnit\MuMeasureUnit;

class MuMeasureUnitController extends Controller
{
    public function get(Request $request)
    {        
        return MuMeasureUnit::select('id', 'description')
               ->where('mu_measure_unit_types_id', $request->muMeasureUnitTypeId)
               ->get(); 
    }
}
