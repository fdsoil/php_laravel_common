<?php

namespace App\Http\Controllers\MeasureUnit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MeasureUnit\MuContainer;

class MuContainerController extends Controller
{
    public function get()
    {        
        return MuContainer::select('id', 'description')
               ->get(); 
    }
}
