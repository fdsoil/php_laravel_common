<?php

namespace App\Http\Controllers\MeasureUnit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MeasureUnit\MuMeasureUnitType;

class MuMeasureUnitTypeController extends Controller
{
    public function get()
    {        
        return MuMeasureUnitType::select('id', 'description')
               ->get(); 
    }
}
