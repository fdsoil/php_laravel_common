<?php

namespace App\Http\Controllers\GeoLocation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\GeoLocation\ZoneType;

class ZoneTypeController extends Controller
{
    public function get()
    {        
        return ZoneType::select('id', 'description')
               ->get(); 
    }
}
