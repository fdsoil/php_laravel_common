<?php

namespace App\Http\Controllers\GeoLocation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\GeoLocation\City;
use App\GeoLocation\Municipality;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{

    public function get(Request $request)
    {            

      $citiId = Municipality::select('city_id')
          ->find( $request->municipalityId )
          ->city_id;

      return City::select('id', 'description')
          ->where('id', $citiId)
          ->get();
     
    }
    
}
