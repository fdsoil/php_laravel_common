<?php

namespace App\Http\Controllers\GeoLocation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\GeoLocation\DomicileType;

class DomicileTypeController extends Controller
{
    public function get()
    {        
        return DomicileType::select('id', 'description')
               ->get(); 
    }
}
