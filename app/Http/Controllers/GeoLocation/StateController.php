<?php

namespace App\Http\Controllers\GeoLocation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\GeoLocation\State;

class StateController extends Controller
{
    public function get()
    {        
        $states = State::select('id', 'description')->get();
        return response($states, 200);               
    }
}
