<?php

namespace App\Http\Controllers\GeoLocation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\GeoLocation\RouteType;

class RouteTypeController extends Controller
{
    public function get()
    {        
        return RouteType::select('id', 'description')
               ->get(); 
    }
}
