<?php

namespace App\Http\Controllers\GeoLocation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\GeoLocation\Parish;

class ParishController extends Controller
{
    public function get(Request $request)
    {          
        return Parish::select('id', 'description')
            ->where('municipality_id', $request->municipalityId)
            ->get();        
    }
}
