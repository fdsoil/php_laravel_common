<?php

namespace App\GeoLocation;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Parroquia extends Model
{
    public static function get($request)
    {          
        $resource = DB::select(
'SELECT A.id, A.descripcion
FROM lugar_geo.parroquia A
WHERE A.id_municipio = :municipioId', 
        ['municipioId' => $request->municipioId]
        );
        return $resource;        
    }
}
