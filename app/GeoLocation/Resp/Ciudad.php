<?php

namespace App\GeoLocation;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    public static function get($request)
    {          
        $resource = DB::select(
'SELECT id, descripcion FROM lugar_geo.ciudad
WHERE id = (SELECT id_ciudad FROM lugar_geo.municipio WHERE id=:municipioId)', 
        ['municipioId' => $request->municipioId]
        );
        return $resource;        
    }
}
