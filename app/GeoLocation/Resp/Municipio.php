<?php

namespace App\GeoLocation;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    public static function get($request)
    {          
        $resource = DB::select(
'SELECT A.id, A.descripcion, B.descripcion as ciudad
FROM lugar_geo.municipio A
INNER JOIN lugar_geo.ciudad B ON A.id_ciudad = B.id
WHERE A.id_estado = :estadoId', 
        ['estadoId' => $request->estadoId]
        );
        return $resource;        
    }
}
