<?php

namespace App\GeoLocation;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    public static function get ()
    {          
        $resource = DB::select('SELECT id, descripcion FROM lugar_geo.estado');
        return $resource;        
    }
}
