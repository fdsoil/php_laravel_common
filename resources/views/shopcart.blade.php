<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="UTF-8">
    <title></title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    
    

</head>
<body>
    <div class="container">
        <div class="row">
        
            <!-- Elementos generados a partir del JSON -->
            <main id="items" class="col-sm-8 row"></main>
            <!-- Carrito -->
            <aside class="col-sm-4">
                <h2>Carrito</h2>            
                <div class="table-responsive-sm">
                <table class="table table-sm table-striped w-auto table-hover table-bordered" border="0">
                    <thead>
                        <tr class="table-info">
                            <th style="text-align:left;font-weight:normal;">Prod</th>
                            <th style="text-align:right;font-weight:normal;">Prec</th>
                            <th style="text-align:right;font-weight:normal;">Cant</th>
                            <th style="text-align:right;font-weight:normal;">sTot</th>
                            <th style="text-align:left;font-weight:normal;">Elim</th>
                        </tr>
                    </thead>
                    <!-- Elementos del carrito -->
                    <tbody id="carrito"></tbody>
                </table>
                </div>
                <hr>
                <!-- Precio total -->
                <p class="text-right">Total: <span id="total"></span>&dollar;</p>
                <button id="boton-vaciar" class="btn btn-danger">Vaciar</button>
            </aside>
        </div>
        
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/Service.js') }}"></script>
    <script src="{{ asset('js/Number.js') }}"></script>
    <script src="{{ asset('js/ShopChart.js') }}"></script>
</body>
</html>
