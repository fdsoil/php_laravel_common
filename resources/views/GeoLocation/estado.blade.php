<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <link rel="shortcut icon" href="#"/>
    <title>GeoLocal</title>
    <link rel="stylesheet" href="{{ asset('css/pickList/GeoLocation/estados.css') }}">
    <style>
.pickList{
margin: 3% 3% 3% 3%;
padding: 3% 3% 3% 3%;
}
.mainTable{
    position:fixed;
    top:0%;
    right:3%;
    left:3%;
	border:0;
	width:93%;
	font-family: sans-serif; 
}

.tHeaderDes{   
    width:95%; 
    font-size:100%;
    font-weight: normal; 
    background: #c3c3c3;
    color:#fff;
    z-index:2001;
    border: 0px solid #000000;
}

.tHeaderX{
    width:3%;
    font-size:100%;
    font-weight: bold;
    background: #c3c3c3;
    color:#fff;
    cursor: pointer;
    z-index:2001;
    border: 1px solid #fff;
}
    </style>
    
  </head>
  <body>
    <section id="contenido">
      <div width="100%" class="pickList"> 
      <div width="100%">
        <table class="mainTable" border=1>
          <thead>
            <tr>
              <th class='tHeaderDes'>Ubicciòn Geográfica</th>	
              <th class='tHeaderX' title="Cerrar" onclick="closePickList();">X</th>
            </tr>      
          </thead>	
        </table> 
      </div>
      <div id="id_div_0">                    
        <table id="id_table_0"  width="100%" border=0 class='tablaNivel0'>
          <tbody title="Seleccione...">
            @foreach ($estados as $estado) 
            <tr>
              <td>
                <div id="{{ $estado->id}}" onclick="llenarNivel(1,this);">
                  <table>
                    <tr>
       	              <td>-{{ $estado->descripcion}}</td>
			        </tr>
			      </table>
	            </div>
		      </td>
	        </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <input class="data"  type="hidden" name="abc" id="abc"  value="">
      <input class="data"  type="hidden" name="ciudad" id="id_ciudad"  value="">
      <script src="{{ asset('js/app.js') }}"></script>
      <script src="{{ asset('js/Service.js') }}"></script>
      <script src="{{ asset('js/PickList.js') }}"></script>
      <script src="{{ asset('js/pickList/GeoLocation/reqs.js') }}"></script>
      <script src="{{ asset('js/pickList/GeoLocation/pickList.js') }}"></script>
      <script src="{{ asset('js/pickList/GeoLocation/tabla.js') }}"></script>      
      </div>
    </section>
  </body>
</html>
