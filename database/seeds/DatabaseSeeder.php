<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(MunicipalitiesTableSeeder::class);
        $this->call(ParishesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(ZoneTypesTableSeeder::class);
        $this->call(RouteTypesTableSeeder::class);
        $this->call(DomicileTypesTableSeeder::class);        
        $this->call(MuContainerSeeder::class);
        $this->call(MuMeasureUnitTypeSeeder::class);
        $this->call(MuMeasureUnitSeeder::class);
    }
}
