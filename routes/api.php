<?php

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::group(['prefix' => 'geo-location'], function() {
    Route::get('state', 'GeoLocation\StateController@get');
    Route::get('municipality/{stateId}', 'GeoLocation\MunicipalityController@get');
    Route::get('parish/{municipalityId}', 'GeoLocation\ParishController@get');
    Route::get('city/{municipalityId}', 'GeoLocation\CityController@get');
    Route::get('zone-type', 'GeoLocation\ZoneTypeController@get');
    Route::get('route-type', 'GeoLocation\RouteTypeController@get');
    Route::get('domicile-type', 'GeoLocation\DomicileTypeController@get');
    Route::get('{ids}', 'GeoLocation\GeoLocationController@get');    
});

Route::group(['prefix' => 'measure-unit'], function() {
    Route::get('container', 'MeasureUnit\MuContainerController@get');
    Route::get('type', 'MeasureUnit\MuMeasureUnitTypeController@get');
    Route::get('/{muMeasureUnitTypeId}', 'MeasureUnit\MuMeasureUnitController@get');    
});
